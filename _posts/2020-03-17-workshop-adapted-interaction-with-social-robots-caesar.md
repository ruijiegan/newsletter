---
title: "Workshop on Adapted intEraction with SociAl Robots (cAESAR)"
shorttitle: 

venue: "Annual Meeting of the Intelligent User Interfaces Community (IUI)"
date: 2020-03-17
dateend: 2020-03-20
location: Cagliari, Italy
link: 
organizers: B. De Carolis, C. Gena, A. Lieto, S. Rossi & A. Sciutti
tags: [workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

