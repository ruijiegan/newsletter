---
title: "5th Latin-American Summer School in Computational Neuroscience (LACONEU)"
shorttitle: 

venue: 
date: 2019-01-07
dateend: 2019-01-25
location: Valparaíso, Chile
link: http://laconeu.cl/
organizers: María José Escobar, Patricio Orio and Wael El-Deredy
tags: [Summer School]
submissions: 
accepted: 
speakers: 
participants: 
---

