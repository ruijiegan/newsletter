---
title: "10th Joint IEEE International Conference on Development and Learning and on Epigenetic Robotics (ICDL-EpiRob)"
shorttitle:

journal: 
date: 2020-10-28
dateend: 2020-10-30
location: Valparaíso, Chile
link: https://cdstc.gitlab.io/icdl-2020/
organizers: Giulio Sandini, Javier Ruiz-del-Solar, Nicolás Navarro-Guerrero, María-José Escobar, Minoru Asada, Frédéric Alexandre, Linda Smith, Angelo Cangelosi, Emre Ugur, Yukie Nagai, Maya Cakmak, Carmelo Bastos, Pablo Barros, Haian Wu, Miguel Solis, Francisco Cruz, Cristóbal Nettle, Patricio Castillo, and Mauricio Araya
tags: [conference]
submissions: 
accepted: 
speakers: 
participants: 
---

