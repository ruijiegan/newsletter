---
title: "HUMANOIDS Workshop on 'From Robotic Dexterous Manipulation to Manual Intelligence'"
shorttitle: 

venue: "IEEE-RAS International Conference on Humanoid Robots (HUMANOIDS)"
date: 2018-11-06
location: Beijing, China
link: https://ni.www.techfak.uni-bielefeld.de/ICHR2018WS/home
organizers: Qiang Li, Zhaopeng Chen, Junpei Zhong, Chenguang Yang and Helge Ritter
tags: [Workshops]
submissions: 
accepted: 
speakers: 
participants: 
---


Workshop on "From Robotic Dexterous Manipulation to Manual Intelligence" at the IEEE-RAS International Conference on Humanoid Robots (HUMANOIDS)

