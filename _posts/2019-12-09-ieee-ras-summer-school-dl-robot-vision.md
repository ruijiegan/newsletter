---
title: "IEEE RAS International Summer School on Deep Learning for Robot Vision"
shorttitle: 

journal: 
date: 2019-12-09
dateend: 2019-12-13
location: Santiago & Rancagua, Chile
link: http://robotvision2019.amtc.cl/
organizers: Rodrigo Verschae, Javier Ruiz-del-Solar, Jens Kober, Martin Adams, Omar Daud, Nicolás Navarro-Guerrero, Daniel Casagrande, Jose Delpino, and Claudio Baeza
tags: [Summer School]
submissions: 
accepted: 
speakers: 
participants: 
---

