---
title: "IROS Workshop on BODIS: The utility of body, interaction and self learning in robotics"
shorttitle: 

venue: "IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS)"
date: 2018-10-01
location: Madrid, Spain
link: https://www.ics.ei.tum.de/en/selfception/bodis-iros2018/
organizers: Pablo Lanillos, Matej Hoffmann, Jun Tani, Giulio Sandini and Gordon Cheng
tags: [workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

Alessandra Sciutti presented at the "Workshop on BODIS: The utility of body, interaction and self learning in robotics" at the IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS)

