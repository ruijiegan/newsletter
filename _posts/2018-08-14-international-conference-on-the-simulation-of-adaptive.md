---
title: "International Conference on the Simulation of Adaptive Behavior (SAB)"
shorttitle: 

date: 2018-08-14
dateend: 2018-08-17
location: Frankfurt, Germany  
link: https://indico.fias.uni-frankfurt.de/event/8/
organizers: Jochen Triesch, John Hallam, Poramate Manoonpong and Jørgen Christian Larsen  
tags: [conference]
submissions: 
accepted: 
speakers: 
participants: 
---


  
