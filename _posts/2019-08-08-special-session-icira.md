---
title: "Special Session Towards robotic dexterous manipulation by multi-modality sensing fusion and control"
shorttitle: 

venue: "International Conference on Intelligent Robotics and Applications (ICIRA)"
journal: 
date: 2019-08-08
dateend: 2019-08-11
location: Shenyang, China
link: https://www.icira2019.org/special-session
organizers: Qiang Li, Zhaopeng Chen, Junpei Zhong, Qifeng Zhang, Shaowei Fan, Huaping Liu
tags: [Special Session]
submissions: 6
accepted: 6
speakers: 
participants: 
---

