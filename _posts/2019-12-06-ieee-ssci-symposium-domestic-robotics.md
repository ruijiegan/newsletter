---
title: "IEEE SSCI Symposium on Domestic Robotics"
shorttitle: 

venue: "IEEE Symposium Series on Computational Intelligence"
journal: 
date: 2019-12-06
dateend: 2019-12-09
location: Xiamen, China
link: http://ssci2019.org/dr.html
organizers: Angelo Cangelosi, Ting Han, Xiaofeng Liu, Ahmad Lotfi and Junpei Zhong
tags: [Symposium]
submissions: 9
accepted: 9
speakers: 
participants: 
---

