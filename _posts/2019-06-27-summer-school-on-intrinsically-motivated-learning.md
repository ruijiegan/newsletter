---
title: "International Summer School on Intrinsically Motivated Open-Ended Learning (IMOL)"
shorttitle: 

venue: 
date: 2019-06-27
dateend: 2019-07-03
location: Frankfurt am Main, Germany
link: http://www.goal-robots.eu/announcements/events/summer-school-2019-2/
organizers: Vieri Giuliano Santucci, Kathryn Merrick, Jochen Triesch, Gianluca Baldassarre
tags: [Summer School, workshop]
submissions: 
accepted: 
speakers: 28
participants: 35
---

