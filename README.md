# CDS Newsletter
## IEEE CIS Technical Committee on Cognitive and Developmental Systems

https://cdstc.gitlab.io/newsletter/

Copyright (C) 2020 [Nicolas Navarro-Guerrero](https://nicolas-navarro-guerrero.github.io/)  
Contact:  nicolas.navarro.guerrero@gmail.com  
        https://nicolas-navarro-guerrero.github.io/


This site was created using [Jekyll](https://jekyllrb.com/), [Bootstrap-4.3.1](https://getbootstrap.com/) and [Font Awesome 5.6.3](https://fontawesome.com/). It is hosted on [GitLab](https://about.gitlab.com/). Patches, suggestions, and comments would be very welcome.

